<?php

use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;
use Phalcon\Http\Request;

class IndexController extends Controller
{

	public function indexAction()
	{
        // Add some local CSS resources
        $this->assets->addCss('//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css');
        $this->assets->addCss('jquery-tags-input/jquery.tagsinput.min.css');
        $this->assets->addCss('//fonts.googleapis.com/css?family=Raleway:400,700');
        $this->assets->addCss('//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css');
        $this->assets->addCss('style.css');

        // And some local JavaScript resources
        $this->assets->addJs('//code.jquery.com/jquery-1.11.1.min.js');
        $this->assets->addJs('jquery-tags-input/jquery.tagsinput.min.js');
        $this->assets->addJs('//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js');
	}


	public function pertanyaanAction()
	{
		$response = new Response();

		$pertanyaan = new Pertanyaan();

		// Store and check for errors
		$success = $pertanyaan->save(
			$this->request->getPost()
		);

		if ($success) {
			return $response->redirect();
		} else {
			echo "Sorry, the following problems were generated: ";
			foreach ($pertanyaan->getMessages() as $message) {
				echo $message->getMessage(), "<br/>";
			}
		}
	}

	public function editAction($id)
	{
        // Add some local CSS resources
        $this->assets->addCss('//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css');
        $this->assets->addCss('jquery-tags-input/jquery.tagsinput.min.css');
        $this->assets->addCss('//fonts.googleapis.com/css?family=Raleway:400,700');
        $this->assets->addCss('//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css');
        $this->assets->addCss('style.css');

        // And some local JavaScript resources
        $this->assets->addJs('//code.jquery.com/jquery-1.11.1.min.js');
        $this->assets->addJs('jquery-tags-input/jquery.tagsinput.min.js');
        $this->assets->addJs('//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js');
	    if (!$this->request->isPost()) {
	        $pertanyaan = Pertanyaan::findFirstById($id);

	        if (!$pertanyaan) {
	            $this->flash->error(
	                "Pertanyaan tidak ditemukan..."
	            );

	            return $this->dispatcher->forward(
	                [
	                    "controller" => "pertanyaan",
	                    "action"     => "index",
	                ]
	            );
	        }

	        $this->view->setVar("pertanyaan", $pertanyaan);
	    }
	}

	public function updateAction($id)
	{
		$response = new Response();

		$pertanyaan = Pertanyaan::findFirst($id);

		if ($pertanyaan !== false) {


			$pertanyaan->nama = $this->request->get('nama');
			$pertanyaan->email = $this->request->get('email');
			$pertanyaan->pertanyaan = $this->request->get('pertanyaan');
			$pertanyaan->tags = $this->request->get('tags');
			$pertanyaan->kategori = $this->request->get('kategori');


		    if ($pertanyaan->update() === false) {
		       
		        $messages = $pertanyaan->getMessages();

		        foreach ($messages as $message) {
		            echo $message, "\n";
		        }

		    } else {
		        return $response->redirect();
		    }
		}
	}

	public function hapusAction($id)
	{
		$response = new Response();

		$pertanyaan = Pertanyaan::findFirst($id);

		if ($pertanyaan !== false) {
		    if ($pertanyaan->delete() === false) {
		        echo "Tidak dapat menghapus pertanyaa: \n";

		        $messages = $pertanyaan->getMessages();

		        foreach ($messages as $message) {
		            echo $message, "\n";
		        }
		    } else {
		        return $response->redirect();
		    }
		}
	}

}
