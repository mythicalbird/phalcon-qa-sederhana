<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Behavior\Timestampable;

class Pertanyaan extends Model
{

	public $id;
	public $nama;
    public $email;
	public $pertanyaan;
	public $kategori;
	public $tanggal;



    public function initialize()
    {
        $this->addBehavior(
            new Timestampable(
                [
                    "beforeCreate" => [
                        "field"  => "tanggal",
                        "format" => "Y-m-d H:i:s",
                    ]
                ]
            )
        );
    }

}
